from django.urls import path, include
from apps.comics.views import index

urlpatterns = [
    path('index', index),
    path('comics/', include('apps.comics.urls')),
]