from django.urls import path

from apps.comics.views import list_comic, view_comic

app_name = 'comics'

urlpatterns = [

    path('comic/', view_comic, name='comic'),
    path('comic/<str:uid>', view_comic, name='comic'),
    path('list_comic', list_comic, name='list_comic'),
]
